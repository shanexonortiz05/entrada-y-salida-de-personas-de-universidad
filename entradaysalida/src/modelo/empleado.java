
package modelo;



public class empleado extends  persona {

    empleado siguiente;
    empleado anterior;
    private String Ttrabajo;
    
    public empleado(int Identificacion, int codigo, String nombre,String Ttrabajo, 
            boolean entrada, String apellido, String direccion, String telefono) {
        super(Identificacion, codigo, nombre, entrada, apellido, direccion, telefono);
    }    

    public empleado getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(empleado siguiente) {
        this.siguiente = siguiente;
    }

    public empleado getAnterior() {
        return anterior;
    }

    public void setAnterior(empleado anterior) {
        this.anterior = anterior;
    }

    public String getTtrabajo() {
        return Ttrabajo;
    }

    public void setTtrabajo(String Ttrabajo) {
        this.Ttrabajo = Ttrabajo;
    }
           
    
}
