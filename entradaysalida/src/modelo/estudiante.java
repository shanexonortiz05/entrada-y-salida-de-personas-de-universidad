
package modelo;


public class estudiante extends persona{

    private String materias;
    estudiante siguiente;
    estudiante anterior;
    
    public estudiante(String materias, int Identificacion, int codigo, String nombre, boolean entrada,
            String apellido, String direccion, String telefono) {
        super(Identificacion, codigo, nombre, entrada, apellido, direccion, telefono);
        this.materias = materias;
    }
    
    public String getMaterias() {
        return materias;
    }

    public void setMaterias(String materias) {
        this.materias = materias;
    }

    public estudiante getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(estudiante siguiente) {
        this.siguiente = siguiente;
    }

    public estudiante getAnterior() {
        return anterior;
    }

    public void setAnterior(estudiante anterior) {
        this.anterior = anterior;
    }

    
    
}
