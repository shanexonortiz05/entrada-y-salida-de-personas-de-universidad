
package modelo;


public class persona {
    private int Identificacion;
    private int codigo;
    private String Nombre;
    private String Apellido;
    private boolean entrada;
    private String direccion;
    private String telefono;
        
    public persona(int Identificacion,int codigo, String nombre, boolean entrada,String apellido, String direccion, String telefono) {
        this.Identificacion = Identificacion;
        this.codigo = codigo;
        this.Nombre = nombre;
        this.entrada = false;
        this.Apellido = apellido;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public int getIdentificacion() {
        return Identificacion;
    }

    public void setIdentificacion(int identificacion) {
        this.Identificacion = identificacion;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        this.Nombre = nombre;
    }
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public boolean isEntrada() {
        return entrada;
    }

    public void setEntrada(boolean entrada) {
        this.entrada = entrada;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
  
    
    
}
