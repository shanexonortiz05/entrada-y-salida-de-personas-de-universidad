/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author steveen
 */
public class entraysale {
    
    entraysale siguiente;
    entraysale anterior;
    private int codigo;
    private boolean dato;
    private String fecha;

    public entraysale(int codigo, boolean dato, String fecha) {
        this.codigo = codigo;
        this.dato = dato;
        this.fecha = fecha;
        this.siguiente= null;
        this.anterior =null;
    }

    public entraysale getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(entraysale siguiente) {
        this.siguiente = siguiente;
    }

    public entraysale getAnterior() {
        return anterior;
    }

    public void setAnterior(entraysale anterior) {
        this.anterior = anterior;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public boolean isDato() {
        return dato;
    }

    public void setDato(boolean dato) {
        this.dato = dato;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
    
    
}
