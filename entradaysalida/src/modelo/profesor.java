/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author steveen
 */
public class profesor extends persona{
    
    private String materias;
    profesor siguiente;
    profesor anterior;
    
    public profesor(String materias, int Identificacion, int codigo, String nombre, boolean entrada,
            String apellido, String direccion, String telefono) {
        super(Identificacion, codigo, nombre, entrada, apellido, direccion, telefono);
        this.materias= materias;
    }

    public String getMaterias() {
        return materias;
    }

    public void setMaterias(String materias) {
        this.materias = materias;
    }

    public profesor getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(profesor siguiente) {
        this.siguiente = siguiente;
    }

    public profesor getAnterior() {
        return anterior;
    }

    public void setAnterior(profesor anterior) {
        this.anterior = anterior;
    }
    
    
    
    
}
