/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import javax.swing.JOptionPane;

import control.*;
import java.util.Calendar;
import javax.swing.ComboBoxModel;
import modelo.*;
import vista.*;

/**
 *
 * @author steveen
 */
public class control extends javax.swing.JFrame {

    int Ce = 0, Cp = 0, Ct = 0;
    manejoestudiantes Me = new manejoestudiantes();
    manejoprofesores Mp = new manejoprofesores();
    manejoempleado Mem = new manejoempleado();
    manejoentraysale Ms = new manejoentraysale();

    public control() {
        initComponents();
        Nombre.setText("P. Nombre");
        Apellido.setText("Apellidos");
        Cedula.setText("Cedula");
        Telefono.setText("Tel");
        Direccion.setText("Direccion");
        Buscar.setText("Codigo");
        Codigo.setText("Codigo");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTree1 = new javax.swing.JTree();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jLayeredPane3 = new javax.swing.JLayeredPane();
        Escoger = new javax.swing.JComboBox<>();
        Codigo = new javax.swing.JTextField();
        Entra = new javax.swing.JButton();
        Sale = new javax.swing.JButton();
        Mostrar = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        Informacion = new javax.swing.JTextPane();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        Nombre = new javax.swing.JTextField();
        Apellido = new javax.swing.JTextField();
        Cedula = new javax.swing.JTextField();
        tipoderegistro = new javax.swing.JComboBox<>();
        insertar = new javax.swing.JButton();
        Direccion = new javax.swing.JTextField();
        Telefono = new javax.swing.JTextField();
        Error = new javax.swing.JLabel();
        Tipos = new javax.swing.JComboBox<>();
        jLayeredPane2 = new javax.swing.JLayeredPane();
        Buscar = new javax.swing.JTextField();
        BBusca = new javax.swing.JButton();
        Tipo = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        Info = new javax.swing.JTextArea();

        jScrollPane2.setViewportView(jTree1);

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        jTabbedPane2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        Escoger.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Eluja Uno", "Profesor", "Estudiante", "Empleado" }));
        Escoger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EscogerActionPerformed(evt);
            }
        });

        Codigo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                CodigoMousePressed(evt);
            }
        });

        Entra.setText("Entra");
        Entra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EntraActionPerformed(evt);
            }
        });

        Sale.setText("Sale");
        Sale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SaleActionPerformed(evt);
            }
        });

        Mostrar.setText("Mostrar");
        Mostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MostrarActionPerformed(evt);
            }
        });

        jScrollPane4.setViewportView(Informacion);

        jLayeredPane3.setLayer(Escoger, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane3.setLayer(Codigo, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane3.setLayer(Entra, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane3.setLayer(Sale, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane3.setLayer(Mostrar, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane3.setLayer(jScrollPane4, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPane3Layout = new javax.swing.GroupLayout(jLayeredPane3);
        jLayeredPane3.setLayout(jLayeredPane3Layout);
        jLayeredPane3Layout.setHorizontalGroup(
            jLayeredPane3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane3Layout.createSequentialGroup()
                .addGroup(jLayeredPane3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jLayeredPane3Layout.createSequentialGroup()
                        .addGap(102, 102, 102)
                        .addComponent(Entra)
                        .addGap(63, 63, 63)
                        .addComponent(Sale))
                    .addGroup(jLayeredPane3Layout.createSequentialGroup()
                        .addGap(172, 172, 172)
                        .addComponent(Mostrar))
                    .addGroup(jLayeredPane3Layout.createSequentialGroup()
                        .addGap(157, 157, 157)
                        .addComponent(Codigo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jLayeredPane3Layout.createSequentialGroup()
                        .addGap(165, 165, 165)
                        .addComponent(Escoger, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jLayeredPane3Layout.createSequentialGroup()
                        .addGap(64, 64, 64)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(66, Short.MAX_VALUE))
        );
        jLayeredPane3Layout.setVerticalGroup(
            jLayeredPane3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane3Layout.createSequentialGroup()
                .addContainerGap(19, Short.MAX_VALUE)
                .addComponent(Escoger, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Codigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Mostrar)
                .addGap(18, 18, 18)
                .addGroup(jLayeredPane3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Entra)
                    .addComponent(Sale))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );

        jTabbedPane2.addTab("E & S", jLayeredPane3);

        Nombre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                NombreMousePressed(evt);
            }
        });
        Nombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NombreActionPerformed(evt);
            }
        });

        Apellido.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                ApellidoMousePressed(evt);
            }
        });

        Cedula.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                CedulaMousePressed(evt);
            }
        });

        tipoderegistro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Elija Uno", "Profesor", "Estudiante", "Empleado" }));
        tipoderegistro.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                tipoderegistroItemStateChanged(evt);
            }
        });

        insertar.setText("Insertar");
        insertar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                insertarActionPerformed(evt);
            }
        });

        Direccion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                DireccionMousePressed(evt);
            }
        });

        Telefono.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                TelefonoMousePressed(evt);
            }
        });

        Error.setForeground(java.awt.Color.red);
        Error.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        Tipos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Elija Uno" }));

        jLayeredPane1.setLayer(Nombre, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(Apellido, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(Cedula, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(tipoderegistro, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(insertar, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(Direccion, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(Telefono, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(Error, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(Tipos, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
        jLayeredPane1.setLayout(jLayeredPane1Layout);
        jLayeredPane1Layout.setHorizontalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jLayeredPane1Layout.createSequentialGroup()
                        .addGap(167, 167, 167)
                        .addComponent(insertar))
                    .addGroup(jLayeredPane1Layout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Nombre, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Cedula, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Direccion, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(51, 51, 51)
                        .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Telefono, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Apellido, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Tipos, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jLayeredPane1Layout.createSequentialGroup()
                        .addGap(139, 139, 139)
                        .addComponent(tipoderegistro, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(63, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jLayeredPane1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(Error, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(77, 77, 77))
        );
        jLayeredPane1Layout.setVerticalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addComponent(tipoderegistro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(Error, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Apellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Nombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Cedula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Telefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Direccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Tipos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                .addComponent(insertar)
                .addGap(46, 46, 46))
        );

        jTabbedPane2.addTab("Insertar", jLayeredPane1);

        Buscar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                BuscarMousePressed(evt);
            }
        });

        BBusca.setText("BUSCAR");
        BBusca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BBuscaActionPerformed(evt);
            }
        });

        Tipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Elija Uno", "Profesor", "Estudiante", "Empleado" }));

        Info.setEditable(false);
        Info.setColumns(20);
        Info.setRows(5);
        Info.setBorder(null);
        jScrollPane1.setViewportView(Info);

        jLayeredPane2.setLayer(Buscar, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(BBusca, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(Tipo, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jScrollPane1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPane2Layout = new javax.swing.GroupLayout(jLayeredPane2);
        jLayeredPane2.setLayout(jLayeredPane2Layout);
        jLayeredPane2Layout.setHorizontalGroup(
            jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jLayeredPane2Layout.createSequentialGroup()
                .addGap(75, 75, 75)
                .addComponent(Buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BBusca)
                .addGap(81, 81, 81))
            .addGroup(jLayeredPane2Layout.createSequentialGroup()
                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jLayeredPane2Layout.createSequentialGroup()
                        .addGap(67, 67, 67)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jLayeredPane2Layout.createSequentialGroup()
                        .addGap(167, 167, 167)
                        .addComponent(Tipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(65, Short.MAX_VALUE))
        );
        jLayeredPane2Layout.setVerticalGroup(
            jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane2Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(Tipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Buscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BBusca))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Busqueda", jLayeredPane2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void NombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NombreActionPerformed

    private void NombreMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_NombreMousePressed
        Nombre.setText("");
    }//GEN-LAST:event_NombreMousePressed

    private void ApellidoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ApellidoMousePressed
        Apellido.setText("");
    }//GEN-LAST:event_ApellidoMousePressed

    private void CedulaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CedulaMousePressed
        Cedula.setText("");
    }//GEN-LAST:event_CedulaMousePressed

    private void TelefonoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TelefonoMousePressed
        Telefono.setText("");
    }//GEN-LAST:event_TelefonoMousePressed

    private void DireccionMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_DireccionMousePressed
        Direccion.setText("");
    }//GEN-LAST:event_DireccionMousePressed

    private void insertarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_insertarActionPerformed
        if (verificar() == false) {
            Error.setText("!!ERROR¡¡ Verirfique la Informacion");
        } else if (tipoderegistro.getModel().getSelectedItem().toString().equalsIgnoreCase("Estudiante")) {
            Ce = Ce + 1;
            vista V = new vista();
            V.mostrar("el codigo del Estudiante es: " + Cestudiate());
            int C = Cestudiate();
            Me.insertar(Nombre.getText(), C, Integer.parseInt(Cedula.getText()), Tipos.getModel().getSelectedItem().toString(), true,
                    Apellido.getText(), Direccion.getText(), Telefono.getText());
            Ms.insertar(C, false, Calendar.getInstance().getTime().toString());
            V.mostrar("Estudisnte Registrado");
            volver();
        } else if (tipoderegistro.getModel().getSelectedItem().toString().equalsIgnoreCase("Profesor")) {
            Cp = Cp + 1;
            vista V = new vista();
            String a = "";
            V.mostrar("el codigo del Profesor es: " + Cprofesor());
            int C = Cprofesor();
            Mp.insertar(Nombre.getText(), C, Integer.parseInt(Cedula.getText()), Tipos.getModel().getSelectedItem().toString(), true,
                    Apellido.getText(), Direccion.getText(), Telefono.getText());
            Ms.insertar(C, false, Calendar.getInstance().getTime().toString());
            V.mostrar("Profesor Registrado");
            volver();
        } else if (tipoderegistro.getModel().getSelectedItem().toString().equalsIgnoreCase("Empleado")) {
            Ct = Ct + 1;
            vista V = new vista();
            String a = "";
            V.mostrar("el codigo del Empleado es: " + Cempleado());
            int C = Cempleado();
            Mem.insertar(Nombre.getText(), C, Integer.parseInt(Cedula.getText()), Tipos.getModel().getSelectedItem().toString(), true,
                    Apellido.getText(), Direccion.getText(), Telefono.getText());
            Ms.insertar(C, false, Calendar.getInstance().getTime().toString());
            V.mostrar("Empleado Registrado");
            volver();
        }
    }//GEN-LAST:event_insertarActionPerformed

    private void volver() {
//        tipoderegistro.removeAllItems();
//        tipoderegistro.addItem("Elija Una");
//        Tipos.removeAllItems();
//        Tipos.addItem("Elija Uno");
        Nombre.setText("P. Nombre");
        Apellido.setText("Apellidos");
        Cedula.setText("Cedula");
        Telefono.setText("Tel");
        Direccion.setText("Direccion");
    }

    private void tipoderegistroItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_tipoderegistroItemStateChanged
        if (tipoderegistro.getModel().getSelectedItem().toString().equalsIgnoreCase("Empleado")) {
            Tipos.removeAllItems();
            Tipos.addItem("Elija Uno");
            Tipos.addItem("Celador");
            Tipos.addItem("P. Aceo");
        } else if (tipoderegistro.getModel().getSelectedItem().toString().equalsIgnoreCase("Estudiante")) {
            Tipos.removeAllItems();
            Tipos.addItem("Elija Uno");
            Tipos.addItem("Ing. Sistemas");
            Tipos.addItem("Ing. Civil");
            Tipos.addItem("Ing Mecanica");
        } else if (tipoderegistro.getModel().getSelectedItem().toString().equalsIgnoreCase("Profesor")) {
            Tipos.removeAllItems();
            Tipos.addItem("Elija Uno");
            Tipos.addItem("Fisica");
            Tipos.addItem("Calculo");
            Tipos.addItem("Poo");
            Tipos.addItem("Bases de Datos");
            Tipos.addItem("Ingles");
        }
    }//GEN-LAST:event_tipoderegistroItemStateChanged

    private void BBuscaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BBuscaActionPerformed
        if (Tipo.getModel().getSelectedItem().toString().equalsIgnoreCase("Estudiante")) {
            if (Me.buscar(Integer.parseInt(Buscar.getText())) == true) {
                Info.setText(Me.Minfo(Integer.parseInt(Buscar.getText())));
            } else {
                Info.setText("no se encuentra Registrado");
            }
        }
        if (Tipo.getModel().getSelectedItem().toString().equalsIgnoreCase("Profesor")) {
            if (Mp.buscar(Integer.parseInt(Buscar.getText())) == true) {
                Info.setText(Mp.Minfo(Integer.parseInt(Buscar.getText())));
            } else {
                Info.setText("no se encuentra Registrado");
            }
        }
        if (Tipo.getModel().getSelectedItem().toString().equalsIgnoreCase("Empleado")) {
            if (Mem.buscar(Integer.parseInt(Buscar.getText())) == true) {
                Info.setText(Mem.Minfo(Integer.parseInt(Buscar.getText())));
            } else {
                Info.setText("no se encuentra Registrado");
            }
        }
    }//GEN-LAST:event_BBuscaActionPerformed

    private void MostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MostrarActionPerformed
        if (Escoger.getModel().getSelectedItem().toString().equalsIgnoreCase("Estudiante") && validaC() == true) {
            if (Me.buscar(Integer.parseInt(Codigo.getText())) == true) {
                String M = Ms.Minfo(Integer.parseInt(Codigo.getText()));
                Informacion.setText(M);
            } else {
                Informacion.setText("el Estudiante no se encuentra registrado");
            }
        }
        if (Escoger.getModel().getSelectedItem().toString().equalsIgnoreCase("Profesor") && validaC() == true) {
            if (Mp.buscar(Integer.parseInt(Codigo.getText())) == true) {
                String M = Ms.Minfo(Integer.parseInt(Codigo.getText()));
                Informacion.setText(M);
            } else {
                Informacion.setText("el Profesor no se encuentra registrado");
            }
        }
        if (Escoger.getModel().getSelectedItem().toString().equalsIgnoreCase("Empleado") && validaC() == true) {
            if (Mem.buscar(Integer.parseInt(Codigo.getText())) == true) {
                String M = Ms.Minfo(Integer.parseInt(Codigo.getText()));
                Informacion.setText(M);
            } else {
                Informacion.setText("el Empleado no se encuentra registrado");
            }
        } 
        if(validaC() == false) {
            JOptionPane.showMessageDialog(null, "¡¡Error!!");
        }
    }//GEN-LAST:event_MostrarActionPerformed

    public boolean validaC() {
        if (Codigo.getText().equalsIgnoreCase("Codigo")) {
            return false;
        }
        return true;
    }

    private void EntraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EntraActionPerformed
        if (Escoger.getModel().getSelectedItem().toString().equalsIgnoreCase("Estudiante") && validaC() == true) {
            if (Me.buscar(Integer.parseInt(Codigo.getText())) == true) {
                if (Me.validarE(Integer.parseInt(Codigo.getText())) == true) {
                    Ms.insertar(Integer.parseInt(Codigo.getText()), true, Calendar.getInstance().getTime().toString());
                    Me.cambiar(Integer.parseInt(Codigo.getText()), true);
                    String m = "Codigo: " + Codigo.getText() + "      "
                            + "Estado: Entro      "
                            + "Fecha: " + Calendar.getInstance().getTime().toString();
                    Informacion.setText(m);
                } else {
                    JOptionPane.showMessageDialog(null, "ya esta andentro");
                }
            }else{
                Informacion.setText("el Estudiante no se encuentra registrado");
            }
        }
        if (Escoger.getModel().getSelectedItem().toString().equalsIgnoreCase("Profesor") && validaC() == true) {
            if (Mp.buscar(Integer.parseInt(Codigo.getText())) == true) {
                if (Mp.validarE(Integer.parseInt(Codigo.getText())) == true) {
                    Ms.insertar(Integer.parseInt(Codigo.getText()), true, Calendar.getInstance().getTime().toString());
                    Mp.cambiar(Integer.parseInt(Codigo.getText()), true);
                    String m = "Codigo: " + Codigo.getText() + "      "
                            + "Estado: Entro      "
                            + "Fecha: " + Calendar.getInstance().getTime().toString();
                    Informacion.setText(m);
                } else {
                    JOptionPane.showMessageDialog(null, "ya esta andentro");
                }
            }else{
                Informacion.setText("el Profesor no se encuentra registrado");
            }
        }
        if (Escoger.getModel().getSelectedItem().toString().equalsIgnoreCase("Empleado") && validaC() == true) {
            if (Mem.buscar(Integer.parseInt(Codigo.getText())) == true) {
                if (Mem.validarE(Integer.parseInt(Codigo.getText())) == true) {
                    Ms.insertar(Integer.parseInt(Codigo.getText()), true, Calendar.getInstance().getTime().toString());
                    Mem.cambiar(Integer.parseInt(Codigo.getText()), true);
                    String m = "Codigo: " + Codigo.getText() + "      "
                            + "Estado: Entro      "
                            + "Fecha: " + Calendar.getInstance().getTime().toString();
                    Informacion.setText(m);
                } else {
                    JOptionPane.showMessageDialog(null, "ya esta andentro");
                }
            }else{
                Informacion.setText("el Empleado no se encuentra registrado");
            } 
        }
        if(validaC() == false){
                JOptionPane.showMessageDialog(null, "¡¡Error!!");
            }
    }//GEN-LAST:event_EntraActionPerformed

    private void BuscarMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BuscarMousePressed
        Buscar.setText("");
    }//GEN-LAST:event_BuscarMousePressed

    private void CodigoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CodigoMousePressed
        Codigo.setText("");
    }//GEN-LAST:event_CodigoMousePressed

    private void SaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SaleActionPerformed
        if (Escoger.getModel().getSelectedItem().toString().equalsIgnoreCase("Estudiante") && validaC() == true) {
            if (Me.buscar(Integer.parseInt(Codigo.getText())) == true) {
                if (Me.validarS(Integer.parseInt(Codigo.getText())) == true) {
                    Ms.insertar(Integer.parseInt(Codigo.getText()), false, Calendar.getInstance().getTime().toString());
                    Me.cambiar(Integer.parseInt(Codigo.getText()), false);
                    String m = "Codigo: " + Codigo.getText() + "      "
                            + "Estado: Salio      "
                            + "Fecha: " + Calendar.getInstance().getTime().toString();
                    Informacion.setText(m);
                } else {
                    JOptionPane.showMessageDialog(null, "ya esta afura");
                }
            }else{
                Informacion.setText("el Estudiante no se encuentra registrado");
            }
        }
        if (Escoger.getModel().getSelectedItem().toString().equalsIgnoreCase("Profesor") && validaC() == true) {
            if (Mp.buscar(Integer.parseInt(Codigo.getText())) == true) {
                if (Mp.validarS(Integer.parseInt(Codigo.getText())) == true) {
                    Ms.insertar(Integer.parseInt(Codigo.getText()),false, Calendar.getInstance().getTime().toString());
                    Mp.cambiar(Integer.parseInt(Codigo.getText()), false);
                    String m = "Codigo: " + Codigo.getText() + "      "
                            + "Estado: Salio      "
                            + "Fecha: " + Calendar.getInstance().getTime().toString();
                    Informacion.setText(m);
                } else {
                    JOptionPane.showMessageDialog(null, "ya esta afura");
                }
            } else{
                Informacion.setText("el Profesor no se encuentra registrado");
            }
        }
        if (Escoger.getModel().getSelectedItem().toString().equalsIgnoreCase("Estudiante") && validaC() == true) {
            if (Mem.buscar(Integer.parseInt(Codigo.getText())) == true) {
                if (Mem.validarE(Integer.parseInt(Codigo.getText())) == true) {
                    Ms.insertar(Integer.parseInt(Codigo.getText()), false, Calendar.getInstance().getTime().toString());
                    Mem.cambiar(Integer.parseInt(Codigo.getText()), false);
                    String m = "Codigo: " + Codigo.getText() + "      "
                            + "Estado: Salio      "
                            + "Fecha: " + Calendar.getInstance().getTime().toString();
                    Informacion.setText(m);
                } else {
                    JOptionPane.showMessageDialog(null, "ya esta afura");
                }
            }else{
                Informacion.setText("el Empleado no se encuentra registrado");
            } 
        }
        if(validaC() == false){
                JOptionPane.showMessageDialog(null, "¡¡Error!!");
            }
    }//GEN-LAST:event_SaleActionPerformed

    private void EscogerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EscogerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EscogerActionPerformed

    public int Cestudiate() {

        int A = Calendar.getInstance().getWeekYear(), Ñ = 15 * Ce;
        String C = String.valueOf(A) + "20" + String.valueOf(Ñ);

        return Integer.parseInt(C);
    }

    public int Cprofesor() {
        int A = Calendar.getInstance().getWeekYear(), Ñ = 15 * Cp;
        String C = String.valueOf(A) + "10" + String.valueOf(Ñ);

        return Integer.parseInt(C);
    }

    public int Cempleado() {
        int A = Calendar.getInstance().getWeekYear(), Ñ = 15 * Ct;
        String C = String.valueOf(A) + "30" + String.valueOf(Ñ);

        return Integer.parseInt(C);
    }

    private boolean verificar() {
        if (Nombre.getText().equalsIgnoreCase("") || Nombre.getText().equalsIgnoreCase("P. Nombre")
                || Apellido.getText().equalsIgnoreCase("") || Apellido.getText().equalsIgnoreCase("Apellidos")
                || Direccion.getText().equalsIgnoreCase("") || Direccion.getText().equalsIgnoreCase("P. Nombre")
                || Telefono.getText().equalsIgnoreCase("") || Telefono.getText().equalsIgnoreCase("P. Nombre")
                || Cedula.getText().equalsIgnoreCase("") || Cedula.getText().equalsIgnoreCase("P. Nombre")
                || tipoderegistro.getModel().getSelectedItem().toString().equalsIgnoreCase("Elija Uno")
                || Tipos.getModel().getSelectedItem().toString().equalsIgnoreCase("Elija Uno")) {
            return false;
        }
        return true;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(control.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(control.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(control.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(control.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new control().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField Apellido;
    private javax.swing.JButton BBusca;
    private javax.swing.JTextField Buscar;
    private javax.swing.JTextField Cedula;
    private javax.swing.JTextField Codigo;
    private javax.swing.JTextField Direccion;
    private javax.swing.JButton Entra;
    private javax.swing.JLabel Error;
    private javax.swing.JComboBox<String> Escoger;
    private javax.swing.JTextArea Info;
    private javax.swing.JTextPane Informacion;
    private javax.swing.JButton Mostrar;
    private javax.swing.JTextField Nombre;
    private javax.swing.JButton Sale;
    private javax.swing.JTextField Telefono;
    private javax.swing.JComboBox<String> Tipo;
    private javax.swing.JComboBox<String> Tipos;
    private javax.swing.JButton insertar;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JLayeredPane jLayeredPane2;
    private javax.swing.JLayeredPane jLayeredPane3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTree jTree1;
    private javax.swing.JComboBox<String> tipoderegistro;
    // End of variables declaration//GEN-END:variables
}
