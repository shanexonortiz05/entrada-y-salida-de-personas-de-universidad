/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import javax.swing.JOptionPane;
import modelo.*;
import vista.*;

/**
 *
 * @author steveen
 */
public class manejoempleado {
    
    empleado primero,ultimo, aux;

    public manejoempleado() {
        primero = null;
        ultimo = null;
    }
   
    public boolean vacia(){
        if(primero == null){
            return true;
        }else{
            return false;
        }
    }
    
    public void insertar(String nombre, int codigo, int identificacion, String Ttrabajo, boolean entrada,
            String apellido, String direccion, String telefono){
        if(vacia()){
            empleado E = new empleado(identificacion, codigo, nombre, Ttrabajo, entrada,apellido, direccion, telefono);
            primero = E;
            ultimo = E;
            E.setSiguiente(E);
            E.setAnterior(E);
        }else{
            empleado E = new empleado( identificacion, codigo, nombre, Ttrabajo, entrada, apellido, direccion, telefono);
            ultimo.setSiguiente(E);
            E.setAnterior(ultimo);
            primero.setAnterior(E);
            E.setSiguiente(primero);
            ultimo = E; 
        }
    }
    
    public void imprimir(){
        String m=" ";
        empleado E = primero;
        
        do{
            m=m+"nombre: "+E.getNombre()+
              "\ncodigo: "+E.getCodigo()+
              "\nidentificacion: "+E.getIdentificacion();
            E = E.getSiguiente();
        }while(E != primero);
        JOptionPane.showMessageDialog(null, m);
    }
    
    public void eliminar(int codigo){
        if(primero !=null){
            empleado aux = primero;
            empleado anterior = null;
            while(aux.getSiguiente() != primero){
                if(aux.getCodigo() == codigo){
                    if(anterior == null){
                        if(aux.getSiguiente() == primero){
                            primero = null;
                        }else{
                            anterior = aux.getAnterior();
                            anterior.setSiguiente(aux.getSiguiente());
                            aux = aux.getSiguiente();
                            aux.setAnterior(anterior);
                            primero = aux;
                            anterior= null;
                        }
                    }else{
                            aux.setAnterior(null);
                            anterior.setSiguiente(aux.getSiguiente());
                            aux = aux.getSiguiente();
                            aux.setAnterior(anterior);
                    }
                }else{
                    anterior = aux;
                    aux = aux.getSiguiente();
                }
            }
        }
    }
    
    public boolean buscar(int codigo){
        empleado aux2 = primero;
        if(vacia()==false){
        do{
            if(aux2.getCodigo()==codigo){
                aux=aux2;
                return true;
            }
            aux2 = aux2.getSiguiente();
        }while(aux2!= primero);
        }
        return false;
    }
    
    public String Minfo(int codigo){
        String M = "el Empleado con codigo "+ String.valueOf(aux.getCodigo())+"\n\n"
                + "    Nombre: "+ aux.getNombre()+"\n"
                + "    Apellido :"+ aux.getApellido()+"\n"
                + "    Identificacion: "+ String.valueOf(aux.getIdentificacion())+"\n"
                + "    Telefono: "+ String.valueOf(aux.getTelefono())+"\n"
                + "    Direccion: "+ aux.getDireccion()+"\n\n";
        if(aux.isEntrada()==true){
            M=M+"El empleado se encuentra dentro de la Institucion";
        }else{
            M=M+"El empleado no se encuentra dentro de le Institucion";
        }
        return M;
    }
    
    public void cambiar(int codigo, boolean dato){
        empleado aux2 = primero;
       
        do{
            if(aux2.getCodigo() == codigo){
                aux2.setEntrada(dato);
            }
            aux2 = aux2.getSiguiente();
        }while(aux2 != primero);
    }
    
    public boolean validarE(int codigo){
        empleado aux2 = primero;
        do{
            if(aux2.getCodigo() == codigo && aux2.isEntrada()==false){
                return true;
            }
            aux2 = aux2.getSiguiente();
        }while(aux2 != primero);
        return false;
    }
    
    public boolean validarS(int codigo){
        empleado aux2 = primero;
        do{
            if(aux2.getCodigo() == codigo && aux2.isEntrada()==true){
                return true;
            }
            aux2 = aux2.getSiguiente();
        }while(aux2 != primero);
        return false;
    }
    
}
