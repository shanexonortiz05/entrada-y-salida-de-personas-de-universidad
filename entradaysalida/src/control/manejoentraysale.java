/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import modelo.*;
/**
 *
 * @author steveen
 */
public class manejoentraysale {
    
    entraysale primero,ultimo, aux; 
    

    public manejoentraysale() {
        primero = null;
        ultimo = null;
    }
   
    public boolean vacia(){
        if(primero == null){
            return true;
        }else{
            return false;
        }
    }
    
    public void insertar(int codigo, boolean entrada, String data){
        if(vacia()){
            entraysale E = new entraysale(codigo, entrada, data);
            primero = E;
            ultimo = E;
            E.setSiguiente(E);
            E.setAnterior(E);
        }else{
            entraysale E = new entraysale(codigo, entrada, data);
            ultimo.setSiguiente(E);
            E.setAnterior(ultimo);
            primero.setAnterior(E);
            E.setSiguiente(primero);
            ultimo = E; 
        }
    }
    
    public void imprimir(){
        String m=" ";
        entraysale E = primero;
        
        do{
            m=m+"Nombre: "+E.getCodigo()+
              "\nCodigo: "+E.getFecha()+
              "\nFecha: "+E.getFecha();
            E = E.getSiguiente();
        }while(E != primero);
    }
    
    public void eliminar(int codigo){
        if(primero !=null){
            entraysale aux = primero;
            entraysale anterior = null;
            while(aux.getSiguiente() != primero){
                if(aux.getCodigo() == codigo){
                    if(anterior == null){
                        if(aux.getSiguiente() == primero){
                            primero = null;
                        }else{
                            anterior = aux.getAnterior();
                            anterior.setSiguiente(aux.getSiguiente());
                            aux = aux.getSiguiente();
                            aux.setAnterior(anterior);
                            primero = aux;
                            anterior= null;
                        }
                    }else{
                            aux.setAnterior(null);
                            anterior.setSiguiente(aux.getSiguiente());
                            aux = aux.getSiguiente();
                            aux.setAnterior(anterior);
                    }
                }else{
                    anterior = aux;
                    aux = aux.getSiguiente();
                }
            }
        }
    }
    
    public boolean buscar(int codigo){
        entraysale aux2 = primero;
        do{
            if(aux2.getCodigo()==codigo){
                aux=aux2;
                return true;
            }
            aux2 = aux2.getSiguiente();
        }while(aux2!= primero);
        return false;
    }
    
    public String Minfo(int codigo){
        String M = "";
        entraysale aux2 = ultimo;
        do{
            if(aux2.getCodigo() == codigo){
                M=M+"Codigo: "+ aux2.getCodigo()+"      ";
                if(aux2.isDato() == true){
                    M=M+"Estado: Entro      "; 
                }else{
                    M=M+"Estado: Salio      ";
                }
                M=M+"Fecha: "+aux2.getFecha()+"\n";
            }
            aux2 = aux2.getAnterior();
        }while(aux2 != ultimo);
        return M;
    }
    
}
